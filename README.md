Central St. Matthew UCC is an open and affirming congregation. We welcome into worship life and membership persons of every race, language, age, gender, sexual orientation, physical and mental ability, and economic level. We commit ourselves to work diligently to end oppression and discrimination.

Address: 1333 S Carrollton Ave, New Orleans, LA 70118, USA

Phone: 504-861-8196

Website: http://www.centralstmatthewucc.com
